# Elo7 Challenge

This is a basic Landing page challenge to re-create the Elo7 careers page.

To run it just do `yarn` to get the dependencies and then `yarn start` to run the project.
You can also see it live at it's [netlify](https://priceless-carson-327151.netlify.com/) link.

## Used libraries
- axios
- styled-components

## Note

Some desired stuff wasn't made due the lack of time during the week, such as responsivity, prop-types and a proper linter integration.
