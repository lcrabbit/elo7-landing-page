import React, { useState, useEffect } from 'react';
import axios from 'axios'
import * as Styled from './styles';

import PersonPhoto from '../../components/PersonPhoto';
import JobOpening from '../../components/JobOpening';
import Benefit from '../../components/Benefit';

const CareersPage = () => {
  const [currentOpenings, setCurrentOpenings] = useState(null);

  useEffect(() => {
    axios.get('https://www.mocky.io/v2/5d6fb6b1310000f89166087b')
      .then(res => {
        setCurrentOpenings(res.data.vagas);
      })
  }, []);

  return (
  <>
    <Styled.GlobalStyle />
    <Styled.Container>
      <Styled.Header>
        <Styled.HeaderTitle>Trabalhe no Elo7</Styled.HeaderTitle>
      </Styled.Header>
      <Styled.HeaderContent>
        <Styled.LightText>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque aliquam consequat. Donec faucibus dui neque. Aliquam erat volutpat. Donec mollis justo vitae urna dictum dignissim eget vitae risus.
        </Styled.LightText>
        <Styled.Separator />
      </Styled.HeaderContent>
      <Styled.Link>Vagas em Aberto »</Styled.Link>
      <Styled.GreyBlock>
        <Styled.CeoBlock>
          <img src={require('../../static/images/placeholder-video.png')} />
          <Styled.CeoBlockColumn>
            <Styled.UppercasedSubtitle>Palavra do Ceo</Styled.UppercasedSubtitle>
            <Styled.CeoName>Carlos Curioni</Styled.CeoName>
            <Styled.GreyText>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque aliquam consequat. Donec faucibus dui neque. Aliquam erat volutpat. Donec mollis justo vitae urna dictum dignissim eget vitae risus.
            </Styled.GreyText>
          </Styled.CeoBlockColumn>
        </Styled.CeoBlock>
        <Styled.UppercasedTitle>
          Conheça nosso time
          <br />
          Fora de série
        </Styled.UppercasedTitle>
        <Styled.PictureContainer>
          <PersonPhoto photoName="camila" />
          <PersonPhoto photoName="guto" />
          <PersonPhoto photoName="david" />
          <PersonPhoto photoName="beatriz" />
        </Styled.PictureContainer>
      </Styled.GreyBlock>
      <Styled.BenefitsContainer>
        <Benefit
          illustrationName="qualidade"
          content={{
            title: 'Qualidade de Vida',
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque aliquam consequat. Donec faucibus dui neque. Aliquam erat volutpat. Donec mollis justo vitae urna dictum dignissim eget vitae risus. Cras non semper nunc, vitae molestie diam.",
          }}
        />
        <Benefit
          illustrationName="descontracao"
          content={{
            title: 'Ambiente Descontraído',
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque aliquam consequat. Donec faucibus dui neque. Aliquam erat volutpat. Donec mollis justo vitae urna dictum dignissim eget vitae risus. Cras non semper nunc, vitae molestie diam. Phasellus vitae nulla vitae mi mattis cursus vitae a augue. Aenean vehicula feugiat vestibulum. Sed sit amet enim et nibh vehicula pretium.",
          }}
        />

        <Benefit
          illustrationName="atividades"
          content={{
            title: 'Atividades Extras',
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque aliquam consequat. Donec faucibus dui neque. Aliquam erat volutpat. Donec mollis justo vitae urna dictum dignissim eget vitae risus. Cras non semper nunc, vitae molestie diam.",
          }}
        />

      </Styled.BenefitsContainer>
      <Styled.Footer>
        <Styled.Separator />
        <Styled.Link>Saiba mais »</Styled.Link>
        <img src={require('../../static/images/foto-bottom.png')} />
        <Styled.UppercasedTitle>Vagas em aberto</Styled.UppercasedTitle>
        <Styled.UppercasedSubtitle>
          Desenvolvimento
        </Styled.UppercasedSubtitle>
        <Styled.OpeningsContainer>
          {currentOpenings && currentOpenings.filter(opening => opening.ativa).map((item, index) => (
            <Styled.OpeningContainer>
              <JobOpening title={item.cargo} where={item.localizacao} onClick={() => window.location.href=`${item.link}`} />
            </Styled.OpeningContainer>
          ))}
        </Styled.OpeningsContainer>
      </Styled.Footer>
    </Styled.Container>
    </>
  );
};

export default CareersPage;
