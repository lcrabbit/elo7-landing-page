import styled, { createGlobalStyle } from 'styled-components';
import fotoHeader from '../../static/images/foto-header.png'

import { colors } from '../../utils';

export const GlobalStyle = createGlobalStyle`
  body {
    display: flex;
    justify-content: center;
    margin: 0;
  }
`;

export const Header = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 450px;
  background-image: url(${fotoHeader});
`;

export const HeaderContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0 48px;
  text-align: center;
`;

export const HeaderTitle = styled.h1`
  font-size: 48px;
  color: white;
  text-shadow: 3px 5px 2px #474747;
`;

export const Link = styled.span`
  display: flex;
  justify-content: center;
  color: ${colors.blue};
  font-weight: 600;
  font-size: 18px;
  text-transform: uppercase;
  cursor: pointer;

  &:hover {
    text-decoration: underline;
  }
`;

export const Separator = styled.div`
  display: flex;
  justify-self: center;
  height: 2px;
  width: 200px;
  background-color: ${colors.lightGrey};
`;

export const GreyBlock = styled.div`
  display: flex;
  flex-direction: column;
  padding: 48px;
  background-color: ${colors.greyBackground};
`;

export const CeoBlock = styled.div`
  display: flex;
  flex-direction: row;
`;

export const LightText = styled.span`
  color: ${colors.lightGrey};
  text-align: justify;
`;

export const GreyText = styled.span`
  color: ${colors.grey};
  text-align: justify;
`;

export const UppercasedSubtitle = styled.span`
  color: ${colors.grey};
  font-weight: 600;
  align-self: flex-start;
  text-transform: uppercase;
`;

export const CeoName = styled.span`
  color: ${colors.yellow};
  font-style: italic;
`;

export const UppercasedTitle = styled.span`
  color: ${colors.grey};
  font-weight: bold;
  text-transform: uppercase;
  font-size: 22px;
`;

export const CeoBlockColumn = styled.div`
  display: flex;
  flex-direction: column;
`;

export const PictureContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-around;
`;

export const BenefitsContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
`;

export const Footer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  align-self: center;
  width: 860px;
`;

export const OpeningContainer = styled.div`
  margin-top: 16px;
`;

export const OpeningsContainer = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 960px;
  justify-content: center;

  ${CeoBlockColumn} {
    margin-left: 52px;
  }

  ${CeoName} {
    margin-top: 4px;
    margin-bottom: 32px;
  }

  ${Link} {
    margin-top: 18px
    margin-bottom: 32px;
  }

  ${HeaderContent} {
    margin-top: 36px;
  }

  ${Separator} {
    margin-top: 32px;
  }

  ${UppercasedTitle} {
    text-align: center;
    align-self: center;
    justify-self: center;
    margin: 52px 0;
  }

  ${BenefitsContainer} {
    margin-top: 62px;
  }

  ${Footer} {
    margin-bottom: 32px;
  }

  ${BenefitsContainer} {
    margin-bottom: 32px;
  }

  ${OpeningsContainer} {
    margin-top: 32px;
  }
`;
