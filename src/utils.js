export const colors = {
  lightGrey: '#9b9ba7',
  greyBackground: '#f8f8f8',
  grey: '#7d7873',
  blue: '#359c9c',
  yellow: '#f9a940',
};
