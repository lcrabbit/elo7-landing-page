import styled from 'styled-components';
import { colors } from '../../utils';

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;

export const Title = styled.span`
  color: ${colors.blue};
  cursor: pointer;

  &:hover {
    text-decoration: underline;
  }
`;

export const Where = styled.span`
  color: ${colors.lightGrey};
`;
