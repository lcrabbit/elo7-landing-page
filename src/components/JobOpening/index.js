import React from 'react';
import * as Styled from './styles';

const JobOpening = ({ title, where, onClick }) => (
  <Styled.Container>
    <Styled.Title onClick={onClick}>{title}</Styled.Title>
    <Styled.Where>{where ? `${where.bairro} - ${where.cidade}, ${where.pais}` : 'Remoto'}</Styled.Where>
  </Styled.Container>
);

export default JobOpening;
