import React from 'react';

const PersonPhoto = ({ photoName, ...props }) => (
  <img
    src={require(`../../static/images/${photoName}.png`)}
    {...props}
  />
);

export default PersonPhoto;
