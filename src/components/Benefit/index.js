import React from 'react';
import * as Styled from './styles';

const Benefit = ({ illustrationName, content }) => (
  <Styled.Container>
    <Styled.Illustration src={require(`../../static/images/${illustrationName}.png`)} />
    <Styled.Title>{content.title}</Styled.Title>
    <Styled.Description>{content.description}</Styled.Description>
  </Styled.Container>
);

export default Benefit;
