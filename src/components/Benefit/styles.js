import styled from 'styled-components';
import { colors } from '../../utils';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  max-height: 470px;
  max-width: 270px;
`;

export const Illustration = styled.img`
  width: 120px;
`;

export const Title = styled.span`
  color: ${colors.grey};
  font-weight: bold;
  font-size: 22px;
  margin: 32px 0;
`;

export const Description = styled.span`
  color: ${colors.lightGrey};
  text-align: justify;
  line-height: 1.4;
`;
